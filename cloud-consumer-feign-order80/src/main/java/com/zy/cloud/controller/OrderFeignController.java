package com.zy.cloud.controller;

import com.zy.cloud.service.PaymentFeignService;
import com.zy.springcloud.entities.CommonResult;
import com.zy.springcloud.entities.Payment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class OrderFeignController {
    @Resource
    private PaymentFeignService paymentFeignService;

    @GetMapping("/consumer/feign/payment/{id}")
    public CommonResult<Payment> queryPaymentById(@PathVariable("id") Long id) {
        return paymentFeignService.queryPaymentById(id);
    }

    @GetMapping(value = "/consumer/feign/timeout")
    public String getPayMentTimeout() {
        return paymentFeignService.getPayMentTimeout();
    }

}
