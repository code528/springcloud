package com.zy.springcloud.controller;

import com.zy.springcloud.entities.CommonResult;
import com.zy.springcloud.entities.Payment;
import com.zy.springcloud.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.TimeUnit;

@RestController
@Slf4j
public class PaymentController {
    @Autowired
    private PaymentService paymentService;

    @Value("${server.port}")
    private String serverPort;

    @PostMapping("/payment/create")
    public CommonResult create(@RequestBody Payment payment) {
        int result = paymentService.create(payment);
        log.info("[PaymentController] create result:{}", result);

        if (result > 0) {
            return CommonResult.builder().code(200).message("插入成功").data(result).build();
        } else {
            return CommonResult.builder().code(400).message("插入失败").data(null).build();
        }
    }

    @GetMapping("/payment/query/{id}")
    public CommonResult queryPaymentById(@PathVariable("id") Long id) {
        Payment payment = paymentService.getPaymentById(id);
        log.info("[PaymentController] queryPaymentById result:{}", payment);
        if (payment != null) {
            return CommonResult.builder()
                    .code(200)
                    .message("查询成功,serverPort:" + serverPort)
                    .data(payment).build();
        } else {
            return CommonResult.builder()
                    .code(404)
                    .message("没有对应的查询记录：" + id + ",serverPort:" + serverPort)
                    .data(null).build();
        }
    }

    @GetMapping(value = "/payment/lb")
    public String getPayMentLb() {
        return serverPort;
    }

    @GetMapping(value = "/payment/feign/timeout")
    public String getPayMentTimeout() {
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return serverPort;
    }

    @GetMapping("/payment/zipkin")
    public String paymentzipkin() {
        return "hi, i'am paymentzipkin server fall back，welcome to zpkin";
    }

}
