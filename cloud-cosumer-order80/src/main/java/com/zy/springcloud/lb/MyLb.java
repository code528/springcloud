package com.zy.springcloud.lb;

import org.springframework.cloud.client.ServiceInstance;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class MyLb implements LoadBalancer {
    private AtomicInteger atomicInteger = new AtomicInteger(0);
    @Override
    public ServiceInstance instance(List<ServiceInstance> serviceInstances) {
        int index = getAndIncrement() % serviceInstances.size();
        return serviceInstances.get(index);
    }

    private int getAndIncrement() {
        int currentVal;
        int nextVal;
        do {
            currentVal = atomicInteger.get();
            nextVal = currentVal >= Integer.MAX_VALUE ? 0 : currentVal + 1;
        } while (!this.atomicInteger.compareAndSet(currentVal, nextVal));
        System.out.println("MyLb getAndIncrement next:" + nextVal);
        return nextVal;
    }
}
