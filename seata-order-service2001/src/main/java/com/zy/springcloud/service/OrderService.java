package com.zy.springcloud.service;

import com.zy.springcloud.domain.Order;

public interface OrderService {
    public void create(Order order);
}
