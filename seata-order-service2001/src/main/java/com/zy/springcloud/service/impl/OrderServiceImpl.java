package com.zy.springcloud.service.impl;

import com.zy.springcloud.dao.OrderDao;
import com.zy.springcloud.domain.Order;
import com.zy.springcloud.service.AccountService;
import com.zy.springcloud.service.OrderService;
import com.zy.springcloud.service.StorageService;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
@Slf4j
public class OrderServiceImpl implements OrderService {
    @Resource
    private OrderDao orderDao;
    @Resource
    private StorageService storageService;
    @Resource
    private AccountService accountService;

    @GlobalTransactional(name = "my_test_tx_group", rollbackFor = Exception.class)
    @Override
    public void create(Order order) {
        log.info("------>开始创建订单");
        orderDao.create(order);
        log.info("----->订单微服务开始调用库存，做扣减");
        storageService.decrease(order.getProductId( ), order.getCount());
        log.info( "----->订单微服务开始调用库存，做扣减end" );

        log.info( "----->订单微服务开始调用账户，做扣减");
        accountService.decrease(order.getUserId(), order.getMoney());
        log.info( "----->订单微服务开始调用账户，做扣减end");

        // 4修改订单状态，从零到i ,1代表已经完成log.info("----->修改订单状态开始");
        log.info( "----->修改订单状态开始");
        orderDao.update(order.getUserId(), 0);
        log.info( "----->修改订单状态结束");
    }
}
