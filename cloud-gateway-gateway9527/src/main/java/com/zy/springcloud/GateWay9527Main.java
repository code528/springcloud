package com.zy.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GateWay9527Main {
    public static void main(String[] args) {
        SpringApplication.run(GateWay9527Main.class, args);
    }
}
