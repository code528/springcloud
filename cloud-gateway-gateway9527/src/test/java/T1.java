import java.time.ZoneId;
import java.time.ZonedDateTime;

public class T1 {
    public static void main(String[] args) {
        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        System.out.println(zonedDateTime);
        // 2021-10-07T17:26:10.382+08:00[Asia/Shanghai]
    }
}
