package com.zy.springcloud.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan({"com.zy.springcloud.dao"})
public class MyBatisConfig {
}
