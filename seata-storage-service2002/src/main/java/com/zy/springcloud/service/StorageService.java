package com.zy.springcloud.service;


import feign.Param;

public interface StorageService {
    //扣减库存信息
    void decrease(Long productId, Integer count);
}

