package com.zy.springcloud.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.zy.springcloud.service.FlowLimitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

@RestController
public class FlowLimitController {
    @Autowired
    private FlowLimitService flowLimitService;
    @GetMapping("/testA")
    public String testA() {
        flowLimitService.doSomeService();
        return "------testA" ;
    }

    @GetMapping( "/testB")
    public String testB() {
        flowLimitService.doSomeService();
        return "------testB";
    }

    @GetMapping( "/testC")
    public String testC() {
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        flowLimitService.doSomeService();
        System.out.println("testC 测试RT");
        return "------testC";
    }
    @GetMapping( "/testD")
    public String testD() {
        System.out.println("testD 测试异常比例");
        int a = 1/0;
        return "------testD";
    }

    @GetMapping("/testHotKey")
    @SentinelResource(value = "testHotKey", blockHandler = "dealTestHotKey")
    public String testHotKey(@RequestParam(value = "p1", required = false) String p1,
                             @RequestParam(value = "p2", required = false) String p2) {
        return "testHotKey";
    }

    public String dealTestHotKey(String p1, String p2, BlockException exception) {
        return "exception dealTestHotKey!!!";
    }
}
