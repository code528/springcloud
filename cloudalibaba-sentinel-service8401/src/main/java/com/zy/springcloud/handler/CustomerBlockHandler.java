package com.zy.springcloud.handler;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.zy.springcloud.entities.CommonResult;

public class CustomerBlockHandler {
    public static CommonResult hadnlerException(BlockException exception) {
        return new CommonResult(444, "按照用户自定义的，global handlerException---1");
    }
    public static CommonResult hadnlerException2(BlockException exception) {
        return new CommonResult(444, "按照用户自定义的，global handlerException---2");
    }
}
