package com.zy.springcloud.service;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Component
public class FlowLimitService {
    @SentinelResource("doSomeService")
    public void doSomeService() {
        System.out.println("doSomeService");
    }
}
