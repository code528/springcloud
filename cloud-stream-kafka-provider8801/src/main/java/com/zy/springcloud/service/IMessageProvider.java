package com.zy.springcloud.service;

public interface IMessageProvider {
    String send();
}
